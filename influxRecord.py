from datetime import datetime
import argparse
import time
import RPi.GPIO as GPIO
import sys
from influxdb import InfluxDBClient

##python3 influxRecord.py --host=deepthought.home --port=8888 --pin=20 --samples=10 --tag=sump
##python3 influxRecord.py --host=deepthought.home --port=8888 --pin=21 --samples=10 --tag=airconditioner

def parse_args():
    parser = argparse.ArgumentParser(description='sample a gpio pin and accumulate number of samples')
    parser.add_argument('--host', nargs='?',
                        help='influx database hostname')
    parser.add_argument('--port', type=int, default=8086, nargs='?',
                        help='influx database port number')
    parser.add_argument('--user', type=str, default='root', nargs='?',
                        help='influx user name')
    parser.add_argument('--password', type=str, default='root', nargs='?',
                        help='influx password')
    parser.add_argument('--database', type=str, default='vibration', nargs='?',
                        help='the destination database')
    parser.add_argument('--tag', type=str, nargs='?',
                        help='tag name associated with the value in influx')
    
    parser.add_argument('--pin', default=21, type=int, nargs='?', help='gpio pin to sample')
    parser.add_argument('--samples', default=8, type=int, nargs='?',
                        help= 'number of samples to average before reporting')
    parser.add_argument('--threshold', default=0.001, type=float, nargs='?',
                        help="sample average that is worth reporting")
    parser.add_argument('--debug', action='store_false')
    return(parser.parse_args())

def printstdout(ct, avgval):
    print("{},{},{},{}".format(datetime.utcnow().isoformat(), ct.tag, ct.samples, avgval))
    sys.stdout.flush()

def influxreport(ct,avgval):
    ##assume ct has connection already
    points =[{
        "measurement": ct.tag,
        "time": datetime.utcnow(),
        "fields": {
            "vibr_avg": avgval,
            "samples": float(ct.samples)
        }}]
    ct.dbh.write_points(points)
        
        
def detected(ct, *actionlist):
    ##build a sample
    ##this is triggered because of a rising edge... so that's one..
    if ct.samples == 0:
        avgsignal = 1.0
    elif ct.samples > 0:
        rng = iter(range(ct.samples))
        accum = 1
        next(rng)
        
        for i in rng:
            accum += GPIO.input(ct.pin)
        avgsignal = accum/ct.samples
    
    if(avgsignal >= ct.threshold):
        [f(ct,avgsignal) for f in actionlist]

def main():
    ctx = parse_args()

    ##stash the influx connection in ctx
    ctx.dbh = InfluxDBClient(ctx.host,ctx.port,ctx.user,ctx.password,ctx.database)

    ##GPIO pin numbering
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ctx.pin,GPIO.IN)
    GPIO.add_event_detect(ctx.pin,
                          GPIO.RISING,
                          lambda x: detected(ctx, *[influxreport]))
    while True:
        time.sleep(0.1)

if __name__ == '__main__':
    main()
