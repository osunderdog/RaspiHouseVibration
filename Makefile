
PIP = pip3
PYTHON = python3
LOGPATH = logs
LOGFILE = $(LOGPATH)/$(shell date --iso=seconds)

SUMPPIN=20
AIRCOND=21

requirements: requirements.txt
	$(PIP) install -r requirements.txt

all: sump central

sump:
	nohup $(PYTHON) influxRecord.py --host=deepthought.home --port=8086 --pin=$(SUMPPIN) --samples=100 --tag=sumppump --threshold=0.01 > $(LOGFILE)-sumppump.log &

central:
	nohup $(PYTHON) influxRecord.py --host=deepthought.home --port=8086 --pin=$(AIRCOND) --samples=100 --tag=airconditioner --threshold=0.01 > $(LOGFILE)-centralair.log &

serviceinstall:
	scp vibration_sensor_airconditioner.service /lib/systemd/system/
	sudo chmod u=rw,g=r,o=r /lib/systemd/system/vibration_sensor_airconditioner.service
	scp vibration_sensor_sumppump.service /lib/systemd/system/
	sudo chmod u=rw,g=r,o=r /lib/systemd/system/vibration_sensor_sumppump.service
