import time

## GPIO  if not available, then should provide some stub
import RPi.GPIO as GPIO

PINS = [20,21]

##GPIO pin numbering
GPIO.setmode(GPIO.BCM)

def detected(pinid):
    print("pin:{}".format(pinid))

def sensorPinSetup(pinid):
    GPIO.setup(pinid,GPIO.IN)
    GPIO.add_event_detect(pinid,GPIO.RISING,detected, 10)
    
    
def main():

    [sensorPinSetup(p) for p in PINS]

    while True:
        time.sleep(0)

if __name__ == '__main__':
    main()
