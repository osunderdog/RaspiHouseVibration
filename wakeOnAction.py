from datetime import datetime
import argparse
import time
import RPi.GPIO as GPIO
import sys

## airconditioner
##
##python3 wakeOnAction.py --pin=20 --samples=8 --threshold=3

## sump pump
##
##python3 wakeOnAction.py --pin=21 --samples=8 --threshold=3

def parse_args():
    parser = argparse.ArgumentParser(description='sample a gpio pin and accumulate number of samples')
    parser.add_argument('--pin', default=21, type=int, nargs='?', help='gpio pin to sample')
    parser.add_argument('--samples', default=8, type=int, nargs='?', help= 'number of samples to accumlate before reporting')
    parser.add_argument('--threshold', default=7, type=int, nargs='?', help="threshold where it's worth logging")
    return(parser.parse_args())

##GPIO pin numbering
GPIO.setmode(GPIO.BCM)

def detected(pinid, samples,thresh):
    ##build a sample
    result = 0
    for i in range(samples):
        bit = GPIO.input(pinid)
        result += bit
    #print(":{:b}".format(result))
    if(result >= thresh):
        print("{},{}".format(datetime.utcnow().isoformat(), result/samples))
        sys.stdout.flush()

def sensorPinSetup(pinid,samples,thresh):
    GPIO.setup(pinid,GPIO.IN)
    GPIO.add_event_detect(pinid,GPIO.RISING,lambda x: detected(x,samples,thresh))
    
    
def main():
    ctx = parse_args()
    sensorPinSetup(ctx.pin, ctx.samples,ctx.threshold)
    while True:
        time.sleep(1)

if __name__ == '__main__':
    main()
