
Quick Raspberry Pi Python script to detect and report sensor vibration values
to InfluxDB

see writeup here:
[Raspberry Pi Vibration Sensor](http://www.wafermovement.com/2018/07/vibrationsensor/)