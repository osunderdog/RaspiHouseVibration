import time
import RPi.GPIO as GPIO
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='sample a gpio pin and accumulate number of bits')
    parser.add_argument('--pin', default=21, type=int, nargs='?', help='gpio pin to sample')
    parser.add_argument('--bits', default=8, type=int, nargs='?', help= 'number of bits to accumlate before reporting')
    return(parser.parse_args())

def main():
    ctx = parse_args()
    
    ##GPIO pin numbering
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ctx.pin,GPIO.IN)

    result = 0
    while True:
        for i in range(ctx.bits):
            result += GPIO.input(ctx.pin)
        print(result)
        result = 0
        

if __name__ == "__main__":
    main()    

